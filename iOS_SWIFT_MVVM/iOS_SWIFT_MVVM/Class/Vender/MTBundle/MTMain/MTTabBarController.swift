//
//  MTTabBarController.swift
//  iOS_SWIFT_MVVM
//
//  Created by Maktub on 2018/1/1.
//  Copyright © 2018年 Maktub. All rights reserved.
//

import UIKit


class MTTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAllChildViewController()
        
        // Do any additional setup after loading the view.
    }
    
    private func setUpAllChildViewController() {
        setUpChildViewController(MTViewController(), image: UIImage.init(named: "购物车2")!, selectedImage: UIImage.init(named: "购物车2")!, title: "购物车2")
    }
    
    private func setUpChildViewController(_ vc:UIViewController, image:UIImage, selectedImage:UIImage, title:String) {
        vc.tabBarItem.title = title;
        vc.tabBarItem.image = image.withRenderingMode(.alwaysOriginal)
        vc.tabBarItem.selectedImage = selectedImage.withRenderingMode(.alwaysOriginal)
        vc.tabBarItem.setTitleTextAttributes([.foregroundColor:UIColor.init(red: 0.1, green: 0.1, blue: 0.1, alpha: 1)], for: .normal)
        vc.tabBarItem.setTitleTextAttributes([.foregroundColor:UIColor.init(red: 0.1, green: 0.1, blue: 0.1, alpha: 1)], for: .selected)
        let nav:MTNavController = MTNavController.init(rootViewController:vc)
        
        addChildViewController(nav)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
