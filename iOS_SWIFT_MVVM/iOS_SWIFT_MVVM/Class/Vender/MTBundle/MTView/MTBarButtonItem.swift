//
//  MTBarButtonItem.swift
//  iOS_SWIFT_MVVM
//
//  Created by Maktub on 2018/1/1.
//  Copyright © 2018年 Maktub. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    public class func mt_barButtonItemWithImage(_ imageName:String, highImageName:String, target:Any, action:Selector) -> UIBarButtonItem! {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: imageName), for: .normal)
        button.setImage(UIImage.init(named: imageName), for: .selected)
        button.sizeToFit()
        button.addTarget(target, action: action, for: .touchUpInside)
        
        
        return UIBarButtonItem.init(customView: button)
    }
}
